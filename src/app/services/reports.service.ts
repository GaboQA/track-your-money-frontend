import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Report } from '../model/reports';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ReportsService {


  private baseUrl = 'http://localhost:8000/api';

  constructor(private http: HttpClient) { }

  list_report() {
    return this.http.get(`${this.baseUrl}/reports`)
  }

  list_total(): Observable<any>{
    return this.http.get(`${this.baseUrl}/last_month`)
  }

  reports_between(report: Report): Observable<any>{
    return this.http.post(`${this.baseUrl}/reports_between`, report)
  }
}

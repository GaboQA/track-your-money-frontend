import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Category } from '../model/category';


@Injectable({
  providedIn: 'root'
})

export class CategoryService {

  private baseUrl = 'http://localhost:8000/api';

  constructor(private http: HttpClient) { }

  list_category() {
    return this.http.get(`${this.baseUrl}/category`)
  }

  add_category(category: Category): Observable<any> {

    return this.http.post(`${this.baseUrl}/category`, category);
  }



  edit_category(category: Category): Observable<any> {
    return this.http.put(`${this.baseUrl}/category`, category);
  }
  deletecategory(id) {
    return this.http.delete(`${this.baseUrl}/category/${id}`)
  }

  mod_category(category: Category, id): Observable<any> {
    return this.http.put(`${this.baseUrl}/category/${id}`, category);
  }

  edit_categories(id) {
    return this.http.get(`${this.baseUrl}/category/${id}`);
  }
}

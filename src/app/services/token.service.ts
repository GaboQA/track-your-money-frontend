import { Injectable } from '@angular/core';

@Injectable()
export class TokenService {
  private iss = {
    login: 'http://localhost:8000/api/login',
    loginsocial: 'http://localhost:8000/api/loginsocial',
    signup: 'http://localhost:8000/api/signup',
    profile: 'http://localhost:8000/api/profile',
    coin: 'http://localhost:8000/api/coin',
    account: 'http://localhost:8000/api/account',
    category: 'http://localhost:8000/api/category',
    transacction: 'http://localhost:8000/api/transacction',
  };

  constructor() { }

  handle(token) {
    this.set(token);
  }

  set(token) {
    localStorage.setItem('token', token);
  }
  get() {
    return localStorage.getItem('token');
  }

  remove() {
    localStorage.removeItem('token');
  }

  isValid() {
    const token = this.get();
    if (token) {
      const payload = this.payload(token);
      if (payload) {
        return Object.values(this.iss).indexOf(payload.iss) > -1 ? true : false;
      }
    }
    return false;
  }

  payload(token) {
    const payload = token.split('.')[1];
    return this.decode(payload);
  }

  decode(payload) {
    return JSON.parse(atob(payload));
  }

  loggedIn() {
    return this.isValid();
  }
}

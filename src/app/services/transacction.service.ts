import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Transacction } from '../model/transacction';
import { Transfer } from '../model/transfer';

@Injectable({
  providedIn: 'root'
})
export class TransacctionService {

  private baseUrl = 'http://localhost:8000/api';

  constructor(private http: HttpClient) { }

  list_transacction() {
    return this.http.get(`${this.baseUrl}/transacction`)
  }

  list_reports(): Observable<any>{
    return this.http.get(`${this.baseUrl}/reports`)
  }


  add_transacction(transacction: Transacction): Observable<any> {

    return this.http.post(`${this.baseUrl}/transacction`, transacction);
  }

  update_transacction(transacction: Transacction): Observable<any> {

    return this.http.put(`${this.baseUrl}/transacction`, transacction);
  }

  edit_transacction(transacction: Transacction): Observable<any> {
    return this.http.put(`${this.baseUrl}/transacction`, transacction);
  }

  add_transfer(transfer: Transfer): Observable<any> {
    return this.http.post(`${this.baseUrl}/transfer`, transfer);
  }
  delete_transacction(id) {
    return this.http.delete(`${this.baseUrl}/transacction/${id}`)
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Coin } from '../model/coin';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CoinService {



  private baseUrl = 'http://localhost:8000/api';

  constructor(private http: HttpClient) { }

  list_coin() {
    return this.http.get(`${this.baseUrl}/coin`)
  }

  add_Coin(coin: Coin): Observable<any> {

    return this.http.post(`${this.baseUrl}/coin`, coin);
  }

  update_coin(coin: Coin, id): Observable<any> {

    return this.http.put(`${this.baseUrl}/coin/${id}`, coin);
  }

  edit_coin(id) {
    return this.http.get(`${this.baseUrl}/coin/${id}`);
  }

  deletecoin(id) {
    return this.http.delete(`${this.baseUrl}/coin/${id}`)
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Account } from "../model/account";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountService {



  private baseUrl = 'http://localhost:8000/api';

  constructor(private http: HttpClient) { }



  list_account() {
    return this.http.get(`${this.baseUrl}/account`)
  }

  add_account(account: Account): Observable<any> {

    return this.http.post(`${this.baseUrl}/account`, account);
  }

  mod_account(account: Account, id): Observable<any> {
    return this.http.put(`${this.baseUrl}/account/${id}`, account);
  }

  delete_account(id): Observable<any>{
    return this.http.delete(`${this.baseUrl}/account/${id}`)
  }

  edit_account(id) {
    return this.http.get(`${this.baseUrl}/account/${id}`);
  }

}

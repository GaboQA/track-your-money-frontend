export interface Coin {
  small_name: string;
  symbol: string;
  description: string;
  rate: number;
  available: boolean;
}

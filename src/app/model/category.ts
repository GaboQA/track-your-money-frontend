export interface Category {
  father_cat: number;
  category: string;
  type: string;
  description: string;
}

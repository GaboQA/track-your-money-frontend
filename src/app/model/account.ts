export interface Account {
  coin:number;
  small_name:string;
  description:string;
  initial_amount:number;
}

export interface Transfer {
  category: string;
  detail: string;
  amount: number;
  account_debited: number;
  accredited_account: number;
}

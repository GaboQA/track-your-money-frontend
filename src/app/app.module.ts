import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { HttpClientModule } from '@angular/common/http';
import { ProfileComponent } from './components/profile/profile.component';
import { TrackmoneyService } from './services/trackmoney.service';
import { TokenService } from './services/token.service';
import { AuthService } from './services/auth.service';
import { AfterLoginService } from './services/after-login.service';
import { BeforeLoginService } from './services/before-login.service';
import { CoinComponent } from './components/coin/coin.component';
import {SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider} from 'ng4-social-login';
import { AccountComponent } from './components/account/account.component';
import { TransacctionComponent } from './components/transacction/transacction.component';
import { CategoryComponent } from './components/category/category.component';
import { ReportsComponent } from './components/reports/reports.component';

const config = new AuthServiceConfig([
{
  id: GoogleLoginProvider.PROVIDER_ID,
  provider: new GoogleLoginProvider('282503100032-fs78kml3v1cg2ifk26svcslk3ofqb40u.apps.googleusercontent.com')
},
{
 id: FacebookLoginProvider.PROVIDER_ID,
 provider: new FacebookLoginProvider('437177667138539')
}
], false);

export function providerConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    SignupComponent,
    ProfileComponent,
    CoinComponent,
    AccountComponent,
    TransacctionComponent,
    CategoryComponent,
    ReportsComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    SocialLoginModule
  ],
  providers: [
    {provide: AuthServiceConfig,useFactory: providerConfig},
    TrackmoneyService,
    TokenService,
    AuthService,
    AfterLoginService,
    BeforeLoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

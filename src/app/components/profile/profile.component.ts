import { Component, OnInit } from '@angular/core';
import {AuthService, SocialUser, GoogleLoginProvider, FacebookLoginProvider} from 'ng4-social-login';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {

  public user: any = SocialUser;
  constructor(private socialAuthService: AuthService){}
    facebooklogin() {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((userData)=>{
      this.user = userData;
    });
  }
    googlelogin(){
      this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((userData)=>{
        this.user=userData; 
      })
    }
}

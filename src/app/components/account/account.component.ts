import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/services/account.service';
import { Account } from 'src/app/model/account';
import { CoinService } from 'src/app/services/coin.service';
import { TrackmoneyService } from 'src/app/services/trackmoney.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  account: Account = {
    coin:null,
    small_name:'',
    description:'',
    initial_amount:null,
  };

  edit_account: Account = {
    coin:null,
    small_name:'',
    description:'',
    initial_amount:null,
  };

  coins: any = [];
  accounts: any = [];
  isError: boolean;
  alert: string;
  isSucces: boolean;

  constructor(private Trackmoney:TrackmoneyService,private coinservice: CoinService,private accountservice: AccountService, private router: Router) { }

  ngOnInit() {
    this.list();
    this.listcoins();
  }

  refresh(){
    this.list();
    this.listcoins();
  }

  list() {
    this.isError = null;
    this.isSucces = null;
      this.accountservice.list_account().subscribe((data) => {
        this.accounts = data;
        console.log(this.accounts);
      }, err => {
        console.log(err.statusText, 'status', err.status);
        this.isError = true;
        this.alert = err.error.err;

      });
  }

  listcoins() {
    this.isError = null;
    this.isSucces = null;
    this.coinservice.list_coin().subscribe((data) => {
      this.coins = data;
      console.log(this.coins);
    }, err => {
      console.log(err.statusText, 'status', err.status);
      this.isError = true;
      this.alert = err.error.err;

    });
}

  addaccount(formaddaccount: NgForm) {
    this.isError = null;
    this.isSucces = null;
    if (formaddaccount.valid) {
        this.accountservice.add_account(this.account).subscribe((data) => {
          console.log(data);
          if (data.status == false) {
            this.isError = true;
            this.alert = data.msg; // get error message
          } else {
            this.isSucces = true;
            this.alert = data.msg;
            this.account = {
              coin:null,
              small_name:'',
              description:'',
              initial_amount:null,
            };
            this.router.navigate(['account']);
            this.refresh();
          }
        }, err => {
          console.log(err.statusText, 'status', err.status);
          this.isError = true;
          this.alert = err.error.err;
        }
      );
    } else {
      console.log('Entro');
      this.isError = true;
      this.alert = 'verify that all fields are full';
    }
  }

  //update account
  modaccount(formmodaccount: NgForm) {
    this.isError = null;
    this.isSucces = null;
    var id = localStorage['idaccount'];
    console.log(formmodaccount);
    if (formmodaccount.valid) {
      this.isError = null;
      this.isSucces = null;
      var id = localStorage['idaccount'];
      this.accountservice.mod_account(this.edit_account, id).subscribe(
        data => {
          if (!data.status) {
            alert('error');
          } else {
            alert('Save succesful');
          }
          location.reload();
          this.refresh();
        },
        err => {
          console.log(err.statusText, 'status', err.status);
          this.isError = true;
          this.alert = err.error.err;
        }
      );
    } else {
      this.isError = true;
      this.alert = 'verify that all fields are full';
    }
  }




  // show modal edit
  edit(id) {
    localStorage['idaccount'] = id;
    this.isError = null;
    this.isSucces = null;
    //console.log(id);
    this.accountservice.edit_account(id).subscribe(
      data => {
        this.edit_account.coin = data[0].coin;
        this.edit_account.small_name = data[0].small_name;
        this.edit_account.description = data[0].description;
        this.edit_account.initial_amount = data[0].initial_amount;
      },
      err => {
        alert(err.error.err);
      }
    );
  }

  deleteaccount(id) {
    this.accountservice.delete_account(id).subscribe(
      data => this.list(),
      error => console.log(error)
    );
    this.refresh();
  }

}

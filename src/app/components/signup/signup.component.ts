import { Component, OnInit } from '@angular/core';
import { TrackmoneyService } from '../../services/trackmoney.service';
import { TokenService } from '../../services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public form = {
    email: null,
    name: null,
    lastname: null,
    telephone: null,
    password: null,
    password_confirmation: null
  };
  public error = [];

  constructor(
    private Trackmoney: TrackmoneyService,
    private Token: TokenService,
    private router: Router
  ) { }

  onSubmit() {
    this.Trackmoney.signup(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }
  handleResponse(data) {
    this.Token.handle(data.access_token);
    this.router.navigateByUrl('/profile');
  }

  handleError(error) {
    this.error = error.error.errors;
  }

  ngOnInit() {
  }

}

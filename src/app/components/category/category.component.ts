import { Component, OnInit } from '@angular/core';
import { CoinService } from 'src/app/services/coin.service';
import { AccountService } from 'src/app/services/account.service';
import { Category } from 'src/app/model/category';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  category: Category = {
    father_cat: null,
    category: '',
    type: '',
    description: '',
  };
  mod_category: Category = {
    father_cat: null,
    category: '',
    type: '',
    description: '',
  };
  types: any[] = [
    {value: 'Expense', viewValue: 'Expense'},
    {value: 'Income', viewValue: 'Income'},
  ];

  categorys: any = [];
  isError: boolean;
  alert: string;
  isSucces: boolean;

  constructor(private coinservice: CoinService, private categoryservice: CategoryService, private accountservice: AccountService,
    private router: Router, private category_service: CategoryService) { }

  ngOnInit() {
    this.list();
  }

  refresh(){
    this.list();
  }
  //list all category
  list() {
    this.isError = null;
    this.isSucces = null;
    this.category_service.list_category().subscribe((data) => {
      this.categorys = data;
      console.log(this.categorys);
    }, err => {
      console.log(err.statusText, 'status', err.status);
      this.isError = true;
      this.alert = err.error.err;

    });
}

// add new expenses or income
  add_category(form_add_category: NgForm) {
    this.isError = null;
    this.isSucces = null;
    console.log(this.category);
    if (form_add_category.valid) {
        this.category_service.add_category(this.category).subscribe((data) => {
          console.log(data);
          if (data.status == false) {
            this.isError = true;
            this.alert = data.msg; // get error message
          } else {
            this.isSucces = true;
            this.alert = data.msg;
            this.category = {
              father_cat: null,
              category: '',
              type: '',
              description: ''
            };
            this.router.navigate(['category']);
            this.refresh();
          }
        }, err => {
          console.log(err.statusText, 'status', err.status, err.msg);
          this.isError = true;
          this.alert = err.msg;
        }
      );
    } else {
      console.log('Entro');
      this.isError = true;
      this.alert = 'verify that all fields are full';
    }
  }
  //update account
  mod_categories(form_mod_category: NgForm) {
    this.isError = null;
    this.isSucces = null;
    console.log(form_mod_category);
    if (form_mod_category.valid) {
      this.isError = null;
      this.isSucces = null;
      var id = localStorage['idcategory'];
      this.categoryservice.mod_category(this.mod_category, id).subscribe(
        data => {
          if (!data.status) {
            alert('error');
          } else {
            alert('Save succesful');
          }
          location.reload();
          this.refresh();
        },
        err => {
          console.log(err.statusText, 'status', err.status);
          this.isError = true;
          this.alert = err.error.err;
        }
      );
    } else {
      this.isError = true;
      this.alert = 'verify that all fields are full';
    }
  }




  // show modal edit
  edit(id) {
    localStorage['idcategory'] = id;
    this.isError = null;
    this.isSucces = null;
    //console.log(id);
    this.categoryservice.edit_categories(id).subscribe(
      data => {
        this.mod_category.father_cat = data[0].father_cat;
        this.mod_category.category = data[0].category;
        this.mod_category.type = data[0].type;
        this.mod_category.description = data[0].description;
      },
      err => {
        alert(err.error.err);
      }
    );
  }

  deletecategory(id) {
    this.categoryservice.deletecategory(id).subscribe(
      data => this.list(),
      error => console.log(error)
    );
  }

}

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ReportsService } from 'src/app/services/reports.service';
import { Report } from 'src/app/model/reports';
import { AccountService } from 'src/app/services/account.service';
import { Account } from 'src/app/model/account';
import { TrackmoneyService } from 'src/app/services/trackmoney.service';
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  report: Report = {
    from: '',
    to: '',
    id_account: null
  };

  account: Account = {
    coin:null,
    small_name:'',
    description:'',
    initial_amount:null,
  };

  expense: string;
  income: string;
  betweem_account: string;

  expense_last_year: string;
  income_last_year: string;

  expense_last_month: string;
  income_last_month: string;

  expense_year: string;
  income_year: string;

  expense_month: string;
  income_month: string;

  coins: any = [];
  accounts: any = [];
  isError: boolean;
  alert: string;
  isSucces: boolean;


  constructor(private Trackmoney:TrackmoneyService,private accountservice: AccountService,private reportsservice: ReportsService, private router: Router) { }

  ngOnInit() {
    this.list();
    this.total();

  }

  refresh(){
    this.list();
  }
  list() {
    this.isError = null;
    this.isSucces = null;
      this.accountservice.list_account().subscribe((data) => {
        this.accounts = data;
        console.log(this.accounts);
      }, err => {
        console.log(err.statusText, 'status', err.status);
        this.isError = true;
        this.alert = err.error.err;

      });
  }

  total() {
    this.isError = null;
    this.isSucces = null;
      this.reportsservice.list_total().subscribe((data) => {
        this.expense_last_year = data.reports_expense_year;
        this.income_last_year = data.reports_income_year;

        this.expense_last_month = data.reports_expense_month;
        this.income_last_month = data.reports_income_month;
      }, err => {
        console.log(err.statusText, 'status', err.status);
        this.isError = true;
        this.alert = err.error.err;

      });
  }

  //list all accounts
  listaccounts() {
    this.isError = null;
    this.isSucces = null;
    this.accountservice.list_account().subscribe((data) => {
      this.accounts = data;
      console.log(this.accounts);
    }, err => {
      console.log(err.statusText, 'status', err.status);
      this.isError = true;
      this.alert = err.error.err;

    });
  }

  reports_between(formreport_betweem: NgForm) {
    this.isError = null;
    this.isSucces = null;
    if (formreport_betweem.valid) {
        this.reportsservice.reports_between(this.report).subscribe((data) => {
          console.log(data);
          if (data.status == false) {
            this.isError = true;
            this.alert = data.msg; // get error message
          } else {
            this.isSucces = true;
            this.alert = data.msg;
            this.expense= data.reports_expense;
            this.income= data.reports_income;
            this.betweem_account = data.account;
            this.report = {
              from: '',
              to: '',
              id_account: null
            };
            //this.router.navigate(['coin']);
            this.refresh();
          }
        }, err => {
          console.log(err.statusText, 'status', err.status);
          this.isError = true;
          this.alert = err.error.err;
        }
      );
    } else {
      console.log('Entro');
      this.isError = true;
      this.alert = 'verify that all fields are full';
    }
  }
}

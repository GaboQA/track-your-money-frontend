import { Component, OnInit } from '@angular/core';
import { Transacction } from 'src/app/model/transacction';
import { Router } from '@angular/router';
import { CoinService } from 'src/app/services/coin.service';
import { AccountService } from 'src/app/services/account.service';
import { NgForm } from '@angular/forms';
import { TransacctionService } from 'src/app/services/transacction.service';
import { CategoryService } from 'src/app/services/category.service';
import { Transfer } from 'src/app/model/transfer';

@Component({
  selector: 'app-transacction',
  templateUrl: './transacction.component.html',
  styleUrls: ['./transacction.component.css']
})
export class TransacctionComponent implements OnInit {

  transacction: Transacction = {
    category: null,
    detail: '',
    amount: null,
    id_account: null
  };

  acc_transfer: Transfer = {
    category: 'Transfer',
    detail: '',
    amount: null,
    account_debited: null,
    accredited_account: null
  };

  types: any[] = [
    {value: 'Expense', viewValue: 'Expense'},
    {value: 'Income', viewValue: 'Income'},
  ];

  accounts: any = [];
  categorys: any = [];
  transacctions: any = [];
  reports_imcome:string;
  reports_expense:string;
  isError: boolean;
  alert: string;
  isSucces: boolean;

  constructor(private coinservice: CoinService,private accountservice: AccountService,
    private router: Router, private category_service: CategoryService,
    private transacction_service: TransacctionService) { }

  ngOnInit() {
    this.list_categorys();
    this.listaccounts();
    this.list();
    //this.list_reports();
  }

refresh(){
  this.list_categorys();
  this.listaccounts();
  this.list();
  //this.list_reports();
  }

  list() {
    this.isError = null;
    this.isSucces = null;
    this.transacction_service.list_transacction().subscribe((data) => {
      this.transacctions = data;
      console.log(this.transacctions);
    }, err => {
      console.log(err.statusText, 'status', err.status);
      this.isError = true;
      this.alert = err.error.err;

    });
  }


 /* list_reports() {
    this.isError = null;
    this.isSucces = null;
    this.transacction_service.list_reports().subscribe((data) => {
    this.reports_imcome = data.reports_income;
    this.reports_expense = data.reports_expense;

    }, err => {
      console.log(err.statusText, 'status', err.status);
      this.isError = true;
      this.alert = err.error.err;

    });
  }*/

  //list all accounts
  listaccounts() {
    this.isError = null;
    this.isSucces = null;
    this.accountservice.list_account().subscribe((data) => {
      this.accounts = data;
      console.log(this.accounts);
    }, err => {
      console.log(err.statusText, 'status', err.status);
      this.isError = true;
      this.alert = err.error.err;

    });
  }

  //list all category
  list_categorys() {
    this.isError = null;
    this.isSucces = null;
    this.category_service.list_category().subscribe((data) => {
      this.categorys = data;
      console.log(this.categorys);
    }, err => {
      console.log(err.statusText, 'status', err.status);
      this.isError = true;
      this.alert = err.error.err;

    });
}

// add new expenses or income
  add_transacction(form_add_transacction: NgForm) {
    this.isError = null;
    this.isSucces = null;

    if (form_add_transacction.valid) {
        this.transacction_service.add_transacction(this.transacction).subscribe((data) => {
          console.log(data);
          if (data.status == false) {
            this.isError = true;
            this.alert = data.msg; // get error message
          } else {
            this.isSucces = true;
            this.alert = data.msg;
            this.transacction = {
              category: null,
              detail: '',
              amount: null,
              id_account: null
            };
            this.router.navigate(['transacction']);
            this.refresh();
          }
        }, err => {
          console.log(err.statusText, 'status', err.status, err.msg);
          this.isError = true;
          this.alert = err.msg;
        }
      );
    } else {
      console.log('Entro');
      this.isError = true;
      this.alert = 'verify that all fields are full';
    }
  }

  // add new expenses or income
  add_transfer(form_add_transfer: NgForm) {
    this.isError = null;
    this.isSucces = null;
    if (form_add_transfer.valid) {
        this.transacction_service.add_transfer(this.acc_transfer).subscribe((data) => {
          console.log(data);
          if (data.status == false) {
            this.isError = true;
            this.alert = data.msg; // get error message
          } else {
            this.isSucces = true;
            this.alert = data.msg;
            this.acc_transfer = {
              category: 'Transfer',
              detail: '',
              amount: null,
              account_debited: null,
              accredited_account: null
            };
            this.router.navigate(['transacction']);
            this.refresh();
          }
        }, err => {
          console.log(err.statusText, 'status', err.status, err.msg);
          this.isError = true;
          this.alert = err.msg;
        }
      );
    } else {
      console.log('Entro');
      this.isError = true;
      this.alert = 'verify that all fields are full';
    }
  }
  deletetransaccion(id) {
    this.transacction_service.delete_transacction(id).subscribe(
      data => this.list(),
      error => console.log(error)
    );
  }
}

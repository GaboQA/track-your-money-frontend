import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CoinService } from 'src/app/services/coin.service';
import { NgForm } from '@angular/forms';
import { Coin } from 'src/app/model/coin';
import { TrackmoneyService } from 'src/app/services/trackmoney.service';

@Component({
  selector: 'app-coin',
  templateUrl: './coin.component.html',
  styleUrls: ['./coin.component.css']
})
export class CoinComponent implements OnInit {

  symbol_coins: any[] = [
    {small_name: 'CRC', viewValue: 'CRC ₡'},
    {small_name: 'GBP', viewValue: 'GBP £'},
    {small_name: 'JPY', viewValue: 'JPY ¥'},
    {small_name: 'CNY', viewValue: 'CNY ¥'},
    {small_name: 'EUR', viewValue: 'EUR €'},
    {small_name: 'USD', viewValue: 'USD $'},
    {small_name: 'NOK', viewValue: 'NOK Kr'},
    {small_name: 'CHF', viewValue: 'CHF Fr'},
    {small_name: 'KRW', viewValue: 'KRW ₩'},
  ];


  coin: Coin = {
    small_name: '',
    symbol: '',
    description: '',
    rate: null,
    available: null
  };
  mod_coin: Coin = {
    small_name: '',
    symbol: '',
    description: '',
    rate: null,
    available: null
  };
  coins: any = [];
  isError: boolean;
  alert: string;
  isSucces: boolean;

  constructor(private Trackmoney:TrackmoneyService, private coinservice: CoinService, private router: Router) { }

  ngOnInit() {
    this.list();
  }

  refresh(){
    this.list();
  }

  list() {
    this.isError = null;
    this.isSucces = null;
      this.coinservice.list_coin().subscribe((data) => {
        this.coins = data;
        console.log(this.coins);
      }, err => {
        console.log(err.statusText, 'status', err.status);
        this.isError = true;
        this.alert = err.error.err;

      });
  }

  addcoin(formaddcoin: NgForm) {
    this.isError = null;
    this.isSucces = null;
    if (formaddcoin.valid) {
        this.coinservice.add_Coin(this.coin).subscribe((data) => {
          console.log(data);
          if (data.status == false) {
            this.isError = true;
            this.alert = data.msg; // get error message
          } else {
            this.isSucces = true;
            this.alert = data.msg;
            this.coin = {
              small_name: '',
              symbol: '',
              description: '',
              rate: null,
              available: null
            };
            this.router.navigate(['coin']);
            this.refresh();
          }
        }, err => {
          console.log(err.statusText, 'status', err.status);
          this.isError = true;
          this.alert = err.error.err;
        }
      );
    } else {
      console.log('Entro');
      this.isError = true;
      this.alert = 'verify that all fields are full';
    }
  }

  edit(id) {
    localStorage['idcoin'] = id;
    this.isError = null;
    this.isSucces = null;
    this.coinservice.edit_coin(id).subscribe(
      data => {
        this.mod_coin.small_name = data[0].small_name;
        this.mod_coin.symbol = data[0].symbol;
        this.mod_coin.description = data[0].description;
        this.mod_coin.rate = data[0].rate;
      },
      err => {
        alert(err.error.err);
      }
    );
  }

  deletecoin(id) {
    this.coinservice.deletecoin(id).subscribe(
      data => this.list(),
      error => console.log(error)
    );
  }

  update(formmodcoin: NgForm) {
    this.isError = null;
    this.isSucces = null;

    if (formmodcoin.valid) {
      var id = localStorage['idcoin'];
      this.coinservice.update_coin(this.mod_coin, id).subscribe(
        data => {
          if (!data.status) {
            alert('error');
          } else {
            alert('Update succesful');
          }
          location.reload();
          this.refresh();
        },
        err => {
          console.log(err.statusText, 'status', err.status);
          this.isError = true;
          this.alert = err.error.err;
        }
      );
    } else {
      this.isError = true;
      this.alert = 'verify that all fields are full';
    }
  }
}

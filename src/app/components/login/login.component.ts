import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TrackmoneyService } from '../../services/trackmoney.service';
import { TokenService } from '../../services/token.service';
import { Router } from '@angular/router';
//import { AuthService } from '../../services/auth.service';
import { AuthService, SocialUser, GoogleLoginProvider, FacebookLoginProvider} from 'ng4-social-login';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public form = {
    email: null,
    name: null,
    password: null

  };
  public formSocial = {
    email: null,
    name: null,
    lastname: null,
    telephone: null,
    password: null,
    password_confirmation: null

  };
  public user: any = SocialUser;
  public error = null;

  constructor(
    private Trackmoney: TrackmoneyService,
    private Auth: AuthService,
    private router: Router,
    private Token : TokenService,
    private socialAuthService: AuthService
    ) { }
//Login with socials Login (Google and Facebook)
  facebooklogin() {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((data)=>{
      this.user = data;
             //-----------------------------------------
        //console.log(this.form);
        this.form.email = data.email;
        this.form.password = data.id;

        this.Trackmoney.login(this.form).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
        //If not exists here register name and the other field in especific telephone and last name would null
        
       //console.log(data);
       this.formSocial.email = data.email;
        this.formSocial.name = data.name;
        this.formSocial.lastname = 'null';
        this.formSocial.telephone = 0;
        this.formSocial.password = data.id;
        this.formSocial.password_confirmation = data.id;
        //console.log(this.formSocial); 
        this.Trackmoney.signup(this.formSocial).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        )
      
    });
  }
      googlelogin(){
        this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((data)=>{
        this.user=data; 
        //-----------------------------------------
        //console.log(this.form);
        this.form.email = data.email;
        this.form.password = data.id;
        console.log(this.form);

        this.Trackmoney.login(this.form).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
        //If not exists here register name and the other field in especific telephone and last name would null
        
       //console.log(data);
       this.formSocial.email = data.email;
        this.formSocial.name = data.name;
        this.formSocial.lastname = 'null';
        this.formSocial.telephone = 0;
        this.formSocial.password = data.id;
        this.formSocial.password_confirmation = data.id;
        //console.log(this.formSocial); 
        this.Trackmoney.signup(this.formSocial).subscribe(
          data => this.handleResponse(data),
          error => this.handleError(error)
        );
      })
  }
  //Login
  onSubmit() {  
   this.Trackmoney.login(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
   );
   
  }
  //profile
  handleResponse(data) {
    this.Token.handle(data.access_token);
    //this.Auth.changeAuthStatus(true);
    this.router.navigateByUrl('/profile');
  }
  /*Here login with only email and id*/
  handleSocial(data) {
   this.Token.handle(data.access_token);
   this.router.navigateByUrl('/profile');
  }
  handleError(error) {
    this.error = error.error.error;
  }
  ngOnInit() {
  }

}

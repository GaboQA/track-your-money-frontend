import { NgModule } from '@angular/core';
import { RouterModule,Routes  } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ProfileComponent } from './components/profile/profile.component';
import { BeforeLoginService } from './services/before-login.service';
import { AfterLoginService } from './services/after-login.service';
import { CoinComponent } from './components/coin/coin.component';
import { AccountComponent } from './components/account/account.component';
import { TransacctionComponent } from './components/transacction/transacction.component';
import { CategoryComponent } from './components/category/category.component';
import { ReportsComponent } from './components/reports/reports.component';


const appRoutes: Routes = [
{
  path:"login",
  component:LoginComponent,
  canActivate: [BeforeLoginService]

},
{
  path:"loginsocial",
  component:LoginComponent,
  canActivate: [BeforeLoginService]

},
{
  path: 'signup',
  component: SignupComponent,
  canActivate: [BeforeLoginService]
},
{
  path: 'profile',
  component: ProfileComponent,
  canActivate: [AfterLoginService]
},
{
  path: 'coin',
  component: CoinComponent,
  canActivate: [AfterLoginService]
},
{
  path: 'account',
  component: AccountComponent,
  canActivate: [AfterLoginService]
},
{
  path: 'category',
  component: CategoryComponent,
  canActivate: [AfterLoginService]
},
{ path: 'reports',
  component: ReportsComponent,
  canActivate: [AfterLoginService]
},
{
  path: 'transacction',
  component: TransacctionComponent,
  canActivate: [AfterLoginService]
},
{ path: '',
  component:LoginComponent,
  canActivate: [BeforeLoginService]
},
{ path: '**',
  component:LoginComponent,
  canActivate: [BeforeLoginService]
},
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
